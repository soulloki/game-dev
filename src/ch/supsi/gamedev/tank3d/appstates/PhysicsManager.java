package ch.supsi.gamedev.tank3d.appstates;

import ch.supsi.gamedev.tank3d.controls.listeners.CollisionListener;
import ch.supsi.gamedev.tank3d.controls.listeners.PhysicsListener;
import static ch.supsi.gamedev.tank3d.utils.Utils.*;
import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.PhysicsSpace;
import com.jme3.bullet.PhysicsTickListener;
import com.jme3.bullet.collision.PhysicsCollisionEvent;
import com.jme3.bullet.collision.PhysicsCollisionListener;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import java.util.HashSet;
import java.util.Set;

public class PhysicsManager extends AbstractAppState implements PhysicsTickListener, PhysicsCollisionListener {

    private Node rootNode = null;
    private BulletAppState bulletAppState = null;
    private Set<Spatial> spatials = new HashSet<>();

    private void addSpatial(Spatial spatial) {
        bulletAppState.getPhysicsSpace().add(spatial);
        RigidBodyControl rigidBodyControl = spatial.getControl(RigidBodyControl.class);
        rigidBodyControl.setPhysicsLocation(spatial.getWorldTranslation());
        rigidBodyControl.setPhysicsRotation(spatial.getWorldRotation());
        rigidBodyControl.setUserObject(spatial);
    }

    private void removeSpatial(Spatial spatial) {
        bulletAppState.getPhysicsSpace().remove(spatial);
    }

    @Override
    public void initialize(AppStateManager stateManager, Application application) {
        super.initialize(stateManager, application);
        if (application instanceof SimpleApplication) {
            SimpleApplication simpleApplication = (SimpleApplication) application;
            rootNode = simpleApplication.getRootNode();
        }
        bulletAppState = stateManager.getState(BulletAppState.class);
        bulletAppState.getPhysicsSpace().addTickListener(this);
        bulletAppState.getPhysicsSpace().addCollisionListener(this);
    }

    @Override
    public void cleanup() {
        spatials.forEach(bulletAppState.getPhysicsSpace()::remove);
        spatials.clear();
        bulletAppState.getPhysicsSpace().removeTickListener(this);
        bulletAppState.getPhysicsSpace().removeCollisionListener(this);
    }

    @Override
    public void update(float tpf) {
        Set<Spatial> newSpatials = descendantsWithControl(rootNode, RigidBodyControl.class);
        Set<Spatial> addedSpatials = new HashSet<>(newSpatials);
        addedSpatials.removeAll(spatials);
        Set<Spatial> removedSpatials = new HashSet<>(spatials);
        removedSpatials.removeAll(newSpatials);
        spatials = newSpatials;
        addedSpatials.forEach(this::addSpatial);
        removedSpatials.forEach(this::removeSpatial);
    }

    @Override
    public void prePhysicsTick(PhysicsSpace space, float tpf) {
        Set<PhysicsListener> physicsListeners = descendantsControls(rootNode, PhysicsListener.class);
        physicsListeners.stream().filter(PhysicsListener::isEnabled).forEach(pl -> pl.physicsUpdate(tpf));
    }

    @Override
    public void physicsTick(PhysicsSpace space, float tpf) {
    }

    @Override
    public void collision(PhysicsCollisionEvent event) {
        Spatial spatialA = event.getNodeA();
        Spatial spatialB = event.getNodeB();

        CollisionListener controlA = spatialA != null ? spatialA.getControl(CollisionListener.class) : null;
        if (controlA != null && controlA.isEnabled()) {
            Vector3f point = event.getLocalPointB();
            Vector3f normal = event.getNormalWorldOnB();
            controlA.collision(spatialB, point, normal);
        }

        CollisionListener controlB = spatialB != null ? spatialB.getControl(CollisionListener.class) : null;
        if (controlB != null && controlB.isEnabled()) {
            Vector3f point = event.getLocalPointA();
            Vector3f normal = event.getNormalWorldOnB().negate();
            controlB.collision(spatialA, point, normal);
        }
    }
}
