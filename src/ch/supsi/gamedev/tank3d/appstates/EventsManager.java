package ch.supsi.gamedev.tank3d.appstates;

import ch.supsi.gamedev.tank3d.Event;
import ch.supsi.gamedev.tank3d.controls.listeners.EventListener;
import static ch.supsi.gamedev.tank3d.utils.Utils.*;
import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.scene.Node;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

public class EventsManager extends AbstractAppState {

    private final List<Event> events = new ArrayList<>();
    private Node rootNode = null;

    public void addEvent(Event event) {
        events.add(event);
    }

    @Override
    public void initialize(AppStateManager stateManager, Application application) {
        super.initialize(stateManager, application);
        rootNode = Optional.of(application)
                .filter(SimpleApplication.class::isInstance)
                .map(SimpleApplication.class::cast)
                .map(SimpleApplication::getRootNode)
                .orElse(null);
    }

    @Override
    public void update(float tpf) {
        if (rootNode == null) {
            return;
        }
        Set<EventListener> controls = allDescendentControls(rootNode, EventListener.class);
        controls.forEach(c -> events.forEach(c::event));
        events.clear();
    }
}
