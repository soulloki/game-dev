package ch.supsi.gamedev.tank3d.utils;

import static com.jme3.math.FastMath.*;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.jme3.scene.SceneGraphVisitor;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.Control;
import java.util.HashSet;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Utils {
    
    private static Stream<? extends Control> controlStream(Spatial spatial) {
        return IntStream.range(0, spatial.getNumControls())
                .mapToObj(i -> spatial.getControl(i));
    }
    
    public static Node getRootNode(Spatial spatial) {
        while (spatial != null && spatial.getParent() != null) {
            spatial = spatial.getParent();
        }
        return Optional.ofNullable(spatial)
                .filter(Node.class::isInstance)
                .map(Node.class::cast)
                .orElse(null);
    }
    
    public static boolean hasControl(Spatial spatial, Class<? extends Control> controlClass) {
        return Optional.ofNullable(spatial)
                .map(s -> s.getControl(controlClass))
                .filter(Objects::nonNull)
                .isPresent();
    }
    
    public static <T extends Spatial> T getChild(Node parent, String name, Class<T> childClass) {
        if (parent == null) {
            return null;
        }
        return Optional.ofNullable(name)
                .map(parent::getChild)
                .filter(childClass::isInstance)
                .map(childClass::cast)
                .orElse(null);
    }
    
    public static <T extends Control> Set<T> descendantsControls(Spatial spatial, final Class<T> controlClass) {
        final Set<T> results = new HashSet<>();
        spatial.depthFirstTraversal(s -> Optional.of(controlClass)
                .map(s::getControl)
                .filter(Objects::nonNull)
                .ifPresent(results::add));
        
        return results;
    }
    
    public static <T extends Control> Set<T> allDescendentControls(Spatial spatial, final Class<T> controlClass) {
        final Set<T> results = new HashSet<>();
        spatial.depthFirstTraversal(s -> controlStream(s)
                .filter(controlClass::isInstance)
                .map(controlClass::cast).forEach(results::add));
        
        return results;
    }
    
    public static Set<Spatial> descendantsWithControl(Spatial spatial, final Class<? extends Control> controlClass) {
        final Set<Spatial> results = new HashSet<>();
        spatial.depthFirstTraversal(s1 -> Optional.of(s1)
                .filter(s2 -> Utils.hasControl(s2, controlClass))
                .ifPresent(results::add));
        
        return results;
    }
    
    public static <T extends Control> T getAncestorControl(Spatial spatial, Class<T> controlClass) {
        T result = null;
        Node parent = spatial != null ? spatial.getParent() : null;
        while (result == null && parent != null) {
            result = parent.getControl(controlClass);
            parent = parent.getParent();
        }
        return result;
    }
    
    public static void traverseUpwards(Spatial spatial, SceneGraphVisitor sceneGraphVisitor, int maxRecursions) {
        int recursion = 0;
        while (spatial != null && recursion < maxRecursions) {
            sceneGraphVisitor.visit(spatial);
            spatial = spatial.getParent();
            recursion++;
        }
    }
    
    public static Vector2f randomUnitVector2() {
        Vector2f result = new Vector2f();
        float norm2;
        do {
            IntStream.of(0, 1).forEach(i -> result.set(i, nextRandomFloat() * 2.0f - 1.0f));
            norm2 = result.lengthSquared();
        } while (norm2 > 1.0f);
        result.divideLocal(sqrt(norm2));
        return result;
    }
    
    public static Vector3f randomUnitVector3() {
        Vector3f result = new Vector3f();
        float norm2;
        do {
            IntStream.of(0, 1, 2).forEach(i -> result.set(i, nextRandomFloat() * 2.0f - 1.0f));
            norm2 = result.lengthSquared();
        } while (norm2 > 1.0f);
        result.divideLocal(sqrt(norm2));
        return result;
    }
    
    public static float normalize(float value, float min, float max) {
        float result = (value - min) / (max - min);
        return clamp(result, 0.0f, 1.0f);
    }
    
    public static float normalizeAngle(float angle) {
        while (angle < 0.0f) {
            angle += TWO_PI;
        }
        return angle % TWO_PI;
    }
    
    public static float normalizeAngleDeg(float angle) {
        while (angle < 0.0f) {
            angle += 360.0f;
        }
        return angle % 360.0f;
    }
    
    public static float deltaAngle(float startAngle, float endAngle) {
        float leftDeltaAngle = (startAngle - endAngle + TWO_PI) % TWO_PI;
        float rightDeltaAngle = (endAngle - startAngle + TWO_PI) % TWO_PI;
        return leftDeltaAngle < rightDeltaAngle ? -leftDeltaAngle : rightDeltaAngle;
    }
    
    public static float deltaAngleDeg(float startAngle, float endAngle) {
        float leftDeltaAngle = (startAngle - endAngle + 360.0f) % 360.0f;
        float rightDeltaAngle = (endAngle - startAngle + 360.0f) % 360.0f;
        return leftDeltaAngle < rightDeltaAngle ? -leftDeltaAngle : rightDeltaAngle;
    }
    
    public static boolean nonNull(Object... objects) {
        return Stream.of(objects).allMatch(Objects::nonNull);
    }
    
    public static <T> T[] array(T... items) {
        return items;
    }
}
