package ch.supsi.gamedev.tank3d.controls.effectcontrols;

import ch.supsi.gamedev.tank3d.Globals;
import ch.supsi.gamedev.tank3d.appstates.AudioManager;
import static ch.supsi.gamedev.tank3d.utils.Utils.*;
import com.jme3.app.state.AppStateManager;
import com.jme3.audio.AudioNode;
import com.jme3.effect.ParticleEmitter;
import com.jme3.export.InputCapsule;
import com.jme3.export.JmeExporter;
import com.jme3.export.JmeImporter;
import com.jme3.export.OutputCapsule;
import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Node;
import com.jme3.scene.control.AbstractControl;
import java.io.IOException;
import java.util.stream.Stream;

public class ExplosionControl extends AbstractControl implements Cloneable {

    private static enum State {
        IDLE, FIRING, FIRED
    };

    //State
    private State state = State.IDLE;
    private float lifeTime = 0.0f;
    private float time = 0.0f;
    //SceneGraph
    private Node explosion = null;
    private ParticleEmitter fireEmitter = null;
    private ParticleEmitter shockwaveEmitter = null;
    private ParticleEmitter sparksEmitter = null;
    private ParticleEmitter smokeEmitter = null;
    private ParticleEmitter[] emitters = null;
    private AudioNode audio = null;
    //AppStates
    private AudioManager audioManager = null;
    private boolean initialized = false;

    private void playAudioInstance() {
        if (audioManager != null) {
            audioManager.addSound(audio);
        } else {
            audio.playInstance();
        }
    }

    private void updateGravityVector() {
        Vector3f worldUpVector = explosion.getWorldRotation().getRotationColumn(1);
        for (ParticleEmitter emitter : emitters) {
            float length = emitter.getGravity().length();
            emitter.setGravity(worldUpVector.mult(length));
        }
    }

    private float maxHighLife() {
        return Stream.of(emitters)
                .map(ParticleEmitter::getHighLife)
                .reduce(0.0f, Float::max);
    }

    private void initialize() {
        AppStateManager stateManager = Globals.SINGLETON.getStateManager();
        audioManager = stateManager != null ? stateManager.getState(AudioManager.class) : null;
        explosion = spatial instanceof Node ? (Node) spatial : null;
        fireEmitter = getChild(explosion, "fireEmitter", ParticleEmitter.class);
        shockwaveEmitter = getChild(explosion, "shockwaveEmitter", ParticleEmitter.class);
        sparksEmitter = getChild(explosion, "sparksEmitter", ParticleEmitter.class);
        smokeEmitter = getChild(explosion, "smokeEmitter", ParticleEmitter.class);
        audio = getChild(explosion, "audio", AudioNode.class);
        emitters = array(fireEmitter, shockwaveEmitter, sparksEmitter, smokeEmitter);
        initialized = nonNull(explosion, fireEmitter, shockwaveEmitter, sparksEmitter, smokeEmitter, audio);
    }

    @Override
    protected void controlUpdate(float tpf) {

        if (!initialized) {
            initialize();
            return;
        }

        switch (state) {
            case IDLE:
                return;
            case FIRING:
                updateGravityVector();
                Stream.of(emitters).forEach(ParticleEmitter::emitAllParticles);
                lifeTime = maxHighLife();
                playAudioInstance();
                time = 0.0f;
                state = State.FIRED;
                break;
            case FIRED:
                if (time > lifeTime) {
                    explosion.removeFromParent();
                }
                time += tpf;
                break;
        }
    }

    @Override
    protected void controlRender(RenderManager renderManager, ViewPort viewPort) {
    }

    public void fire() {
        state = State.FIRING;
    }

    @Override
    public void read(JmeImporter importer) throws IOException {
        super.read(importer);
        InputCapsule capsule = importer.getCapsule(this);
        state = capsule.readEnum("state", State.class, State.IDLE);
        lifeTime = capsule.readFloat("lifeTime", 0.0f);
        time = capsule.readFloat("time", 0.0f);
    }

    @Override
    public void write(JmeExporter exporter) throws IOException {
        super.write(exporter);
        OutputCapsule capsule = exporter.getCapsule(this);
        capsule.write(state, "state", State.IDLE);
        capsule.write(lifeTime, "lifeTime", 0.0f);
        capsule.write(time, "time", 0.0f);
    }
}
