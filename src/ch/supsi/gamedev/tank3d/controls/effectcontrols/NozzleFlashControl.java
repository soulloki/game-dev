package ch.supsi.gamedev.tank3d.controls.effectcontrols;

import ch.supsi.gamedev.tank3d.Globals;
import ch.supsi.gamedev.tank3d.appstates.AudioManager;
import static ch.supsi.gamedev.tank3d.utils.Utils.*;
import com.jme3.app.state.AppStateManager;
import com.jme3.audio.AudioNode;
import com.jme3.effect.ParticleEmitter;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Node;
import com.jme3.scene.control.AbstractControl;
import java.util.stream.Stream;

public class NozzleFlashControl extends AbstractControl implements Cloneable {

    //AppStates
    private AudioManager audioManager = null;
    //SceneGraph
    private Node nozzleFlash = null;
    private ParticleEmitter directionalFireEmitter = null;
    private ParticleEmitter radialFireEmitter = null;
    private AudioNode audio = null;
    //Transient
    private boolean firing = false;
    private boolean initialized = false;

    private void initialize() {
        AppStateManager stateManager = Globals.SINGLETON.getStateManager();
        audioManager = stateManager != null ? stateManager.getState(AudioManager.class) : null;
        nozzleFlash = spatial instanceof Node ? (Node) spatial : null;
        directionalFireEmitter = getChild(nozzleFlash, "directionalFireEmitter", ParticleEmitter.class);
        radialFireEmitter = getChild(nozzleFlash, "radialFireEmitter", ParticleEmitter.class);
        audio = getChild(nozzleFlash, "audio", AudioNode.class);
        initialized = nonNull(nozzleFlash, directionalFireEmitter, radialFireEmitter, audio);
    }

    private void playAudioInstance() {
        if (audioManager != null) {
            audioManager.addSound(audio);
        } else {
            audio.playInstance();
        }
    }

    @Override
    protected void controlUpdate(float tpf) {
        if (!initialized) {
            initialize();
            return;
        }
        if (firing) {
            playAudioInstance();
            Stream.of(directionalFireEmitter, radialFireEmitter).forEach(ParticleEmitter::emitAllParticles);
            firing = false;
        }
    }

    @Override
    protected void controlRender(RenderManager renderManager, ViewPort viewPort) {
    }

    public void fire() {
        firing = true;
    }
}
