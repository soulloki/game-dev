package ch.supsi.gamedev.tank3d.controls;

import com.jme3.audio.AudioNode;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Node;
import com.jme3.scene.control.AbstractControl;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MusicControl extends AbstractControl implements Cloneable {

    private static enum State {

        STOPPED, PLAYING, TRANSITION
    }

    //Defaults
    private static final float DEFAULT_TRANISTION_DURATION = 5.0f;
    //Properties
    private float transitionDuration = DEFAULT_TRANISTION_DURATION;
    //States
    private State state = State.STOPPED;
    private float transitionTime = 0.0f;
    //SceneGraph
    private List<AudioNode> tracks = Collections.EMPTY_LIST;
    private AudioNode previousTrack = null;
    private AudioNode currentTrack = null;
    private boolean initialized = false;
    
    private void initialize() {
        
        tracks = Stream.of(spatial)
                .filter(Node.class::isInstance)
                .map(Node.class::cast)
                .map(Node::getChildren)
                .flatMap(List::stream)
                .sequential()
                .filter(AudioNode.class::isInstance)
                .map(AudioNode.class::cast)
                .collect(Collectors.toList());
        
        initialized = true;
    }

    private void updateTransition(float tpf) {
        float factor = transitionTime / transitionDuration;
        previousTrack.setVolume(1.0f - factor);
        currentTrack.setVolume(factor);
        transitionTime += tpf;
        if (transitionTime > transitionDuration) {
            previousTrack.stop();
            transitionTime = 0.0f;
            state = State.PLAYING;
        }
    }

    @Override
    protected void controlUpdate(float tpf) {
        
        if (!initialized) {
            initialize();
            return;
        }
        
        switch (state) {
            case STOPPED:
                break;
            case PLAYING:
                break;
            case TRANSITION:
                updateTransition(tpf);
                break;
        }
    }

    @Override
    protected void controlRender(RenderManager renderManager, ViewPort viewPort) {
    }

    public float getTransitionDuration() {
        return transitionDuration;
    }

    public void setTransitionDuration(float transitionDuration) {
        this.transitionDuration = transitionDuration;
    }
    
    public void play() {
        switch (state) {
            case STOPPED:
                if (tracks.size() > 0) {
                    currentTrack = tracks.get(0);
                    currentTrack.play();
                    state = State.PLAYING;
                }
                break;
            case PLAYING:
                break;
            case TRANSITION:
                break;
        }
    }

    public void play(int trackIndex) {
        switch (state) {
            case STOPPED:
                if (tracks.size() > trackIndex) {
                    currentTrack = tracks.get(trackIndex);
                    currentTrack.play();
                    state = State.PLAYING;
                }
                break;
            case PLAYING:
                if (tracks.size() > trackIndex) {
                    previousTrack = currentTrack;
                    currentTrack = tracks.get(trackIndex);
                    currentTrack.play();
                    state = State.TRANSITION;
                }
                break;
            case TRANSITION:
                if (tracks.size() > trackIndex) {
                    previousTrack.stop();
                    previousTrack = currentTrack;
                    currentTrack = tracks.get(trackIndex);
                    currentTrack.play();
                    state = State.TRANSITION;
                }
                break;
        }
    }

    public void stop() {
        switch (state) {
            case STOPPED:
                break;
            case PLAYING:
                currentTrack.stop();
                currentTrack = null;
                state = State.STOPPED;
                break;
            case TRANSITION:
                currentTrack.stop();
                previousTrack.stop();
                currentTrack = null;
                previousTrack = null;
                state = State.STOPPED;
                break;
        }
    }
}
