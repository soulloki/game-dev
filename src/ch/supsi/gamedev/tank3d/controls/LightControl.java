package ch.supsi.gamedev.tank3d.controls;

import com.jme3.export.JmeImporter;
import com.jme3.light.DirectionalLight;
import com.jme3.light.Light;
import com.jme3.light.LightList;
import com.jme3.light.PointLight;
import com.jme3.light.SpotLight;
import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.control.AbstractControl;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

public class LightControl extends AbstractControl implements Cloneable {

    // SceneGraph
    private final Collection<Light> lights = new ArrayList<>();

    private void updateLight(Light light) {
        Vector3f worldTranslation = spatial.getWorldTranslation();
        Vector3f aheadVector = spatial.getWorldRotation().getRotationColumn(2);
        switch (light.getType()) {
            case Directional:
                DirectionalLight directionalLight = (DirectionalLight) light;
                break;
            case Point:
                PointLight pointLight = (PointLight) light;
                pointLight.setPosition(worldTranslation);
                break;
            case Spot:
                SpotLight spotLight = (SpotLight) light;
                spotLight.setPosition(worldTranslation);
                spotLight.setDirection(aheadVector);
                break;
        }
    }

    @Override
    protected void controlUpdate(float tpf) {
        lights.forEach(this::updateLight);
    }

    @Override
    protected void controlRender(RenderManager renderManager, ViewPort viewPort) {
    }

    public Collection<Light> lights() {
        return lights;
    }

    public void removeLights() {
        LightList lightList = spatial.getLocalLightList();
        for (Light light : lightList) {
            spatial.removeLight(light);
        }
    }

    @Override
    public void read(JmeImporter importer) throws IOException {
        super.read(importer);
        LightList lightList = spatial.getLocalLightList();
        for (Light light : lightList) {
            lights.add(light.clone());
        }
    }
}
