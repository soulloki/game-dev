package ch.supsi.gamedev.tank3d.controls.aicontrols.drivercontrol;

import ch.supsi.gamedev.tank3d.Event;
import ch.supsi.gamedev.tank3d.controls.aicontrols.Behaviour;
import ch.supsi.gamedev.tank3d.controls.aicontrols.HelmControl;
import ch.supsi.gamedev.tank3d.controls.aicontrols.NavigationControl;
import ch.supsi.gamedev.tank3d.controls.aicontrols.WarningControl;
import ch.supsi.gamedev.tank3d.controls.aicontrols.WarningControl.Cardinal;
import ch.supsi.gamedev.tank3d.controls.aicontrols.WarningControl.Danger;

public class EvadeBehaviour extends Behaviour {

    private final HelmControl helmControl;
    private final WarningControl warningControl;
    private final NavigationControl navigationControl;
    private Danger dangerousProjectile;

    public EvadeBehaviour(HelmControl helmControl, WarningControl warningControl, NavigationControl navigationControl) {
        this.helmControl = helmControl;
        this.warningControl = warningControl;
        this.navigationControl = navigationControl;
    }

    @Override
    public void start() {
        System.out.println("EvadeBehaviour attivato");
    }

    @Override
    public void loop(float tpf) {
        if (navigationControl.isEnabled()) {
            dangerousProjectile = warningControl.getDanger();
            if (dangerousProjectile != null) {
                helmControl.setSpeed(HelmControl.Speed.FULL);

                Cardinal position = dangerousProjectile.position();
                Cardinal direction = dangerousProjectile.direction();

                switch (direction) {
                    case BACK:
                        switch (position) {
                            case BACK:
                                helmControl.setHeading(0.0f);
                                break;
                            case FRONT:
                                helmControl.setHeading(180.0f);
                                break;
                            case LEFT:
                                helmControl.setHeading(90.0f);
                                break;
                            case RIGHT:
                                helmControl.setHeading(270.0f);
                                break;
                        }
                        break;
                    case FRONT:
                        switch (position) {
                            case BACK:
                                helmControl.setHeading(270.0f);
                                break;
                            case FRONT:
                                helmControl.setHeading(0.0f);
                                break;
                            case LEFT:
                                helmControl.setHeading(90.0f);
                                break;
                            case RIGHT:
                                helmControl.setHeading(270.0f);
                                break;
                        }
                        break;
                    case LEFT:
                        switch (position) {
                            case BACK:
                                helmControl.setHeading(0.0f);
                                break;
                            case FRONT:
                                helmControl.setHeading(180.0f);
                                break;
                            case LEFT:
                                helmControl.setHeading(0.0f);
                                break;
                            case RIGHT:
                                helmControl.setHeading(180.0f);
                                break;
                        }
                        break;
                    case RIGHT:
                        switch (position) {
                            case BACK:
                                helmControl.setHeading(0.0f);
                                break;
                            case FRONT:
                                helmControl.setHeading(180.0f);
                                break;
                            case LEFT:
                                helmControl.setHeading(180.0f);
                                break;
                            case RIGHT:
                                helmControl.setHeading(0.0f);
                                break;
                        }
                        break;
                    default:
                        break;
                }
            } else {
                getManager().setCurrentBehaviour(PatrolBehaviour.class);
            }

        }
    }

    @Override
    public void end() {
    }

    @Override
    public void event(Event event) {
    }
}
