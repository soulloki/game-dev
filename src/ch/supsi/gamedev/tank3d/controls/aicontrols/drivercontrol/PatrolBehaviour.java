package ch.supsi.gamedev.tank3d.controls.aicontrols.drivercontrol;

import ch.supsi.gamedev.tank3d.Event;
import ch.supsi.gamedev.tank3d.controls.aicontrols.Behaviour;
import ch.supsi.gamedev.tank3d.controls.aicontrols.HelmControl;
import ch.supsi.gamedev.tank3d.controls.aicontrols.NavigationControl;
import ch.supsi.gamedev.tank3d.controls.aicontrols.WarningControl;
import static ch.supsi.gamedev.tank3d.utils.Utils.*;
import com.jme3.math.Vector3f;
import java.util.TreeMap;

public class PatrolBehaviour extends Behaviour {

    private final HelmControl helmControl;
    private final WarningControl warningControl;
    private final NavigationControl navigationControl;
    private final Vector3f[] waypoints;
    private int waypointsCount;

    public PatrolBehaviour(HelmControl helmControl, WarningControl warningControl, NavigationControl navigationControl, Vector3f[] waypoints) {
        this.helmControl = helmControl;
        this.warningControl = warningControl;
        this.navigationControl = navigationControl;
        this.waypoints = waypoints;
        this.waypointsCount = 0;
    }

    @Override
    public void start() {
        System.out.println("PatrolBeahaviour attivato");
    }

    @Override
    public void loop(float tpf) {
        if (navigationControl.isEnabled()) {

            if (navigationControl.onCourse()) {
                float approachAzimuth = navigationControl.getApproachAzimuth();
                helmControl.setHeading(approachAzimuth);
                helmControl.setSpeed(HelmControl.Speed.CRUISE);
            } else {
                Vector3f nextWaypoint = waypoints[waypointsCount];
                navigationControl.plotCourse(nextWaypoint);
                helmControl.setHeading(navigationControl.getApproachAzimuth());
                helmControl.setSpeed(HelmControl.Speed.SLOW);

                waypointsCount = (waypointsCount + 1) % waypoints.length;
            }
        }

        WarningControl.Danger danger = warningControl.getDanger();
        if (danger != null) {
            getManager().setCurrentBehaviour(EvadeBehaviour.class);
        }

    }

    @Override
    public void end() {
        //quando torna in questo stato ricomincia il giro
        waypointsCount = 0;
    }

    @Override
    public void event(Event event) {

    }
}

/*public class PatrolBehaviour extends Behaviour {

    private static final TreeMap<Float, HelmControl.Speed> SPEED_MAP = new TreeMap<>();

    static {
        SPEED_MAP.put(0.3f, HelmControl.Speed.SLOW);
        SPEED_MAP.put(0.8f, HelmControl.Speed.CRUISE);
        SPEED_MAP.put(Float.MAX_VALUE, HelmControl.Speed.FULL);
    }
    private final HelmControl helmControl;
    private final WarningControl warningControl;
    private final NavigationControl navigationControl;
    private final Vector3f[] waypoints;
    private int curretWaypointIndex = 0;

    private HelmControl.Speed computeSpeed(float deltaAzimuth, float distance) {
        float azimuthFactor = normalize(deltaAzimuth, 180.0f, 0.0f);
        float distanceFactor = normalize(distance, 0.0f, 200.0f);
        float factor = azimuthFactor * distanceFactor;
        return SPEED_MAP.ceilingEntry(factor).getValue();
    }

    private void nextWaypoint() {
        if (waypoints.length <= 0) {
            return;
        }
        curretWaypointIndex++;
        curretWaypointIndex %= waypoints.length;
        Vector3f waypoint = waypoints[curretWaypointIndex];
        navigationControl.plotCourse(waypoint);
    }

    public PatrolBehaviour(HelmControl helmControl, WarningControl warningControl, NavigationControl navigationControl, Vector3f[] waypoints) {
        this.helmControl = helmControl;
        this.warningControl = warningControl;
        this.navigationControl = navigationControl;
        this.waypoints = waypoints;
        curretWaypointIndex = 0;
    }

    @Override
    public void start() {
        if (waypoints.length <= 0) {
            return;
        }
        Vector3f waypoint = waypoints[curretWaypointIndex];
        navigationControl.plotCourse(waypoint);
    }

    @Override
    public void loop(float tpf) {
        if (navigationControl.onCourse()) {
            float azimuth = navigationControl.getApproachAzimuth();
            helmControl.setHeading(azimuth);
            float distance = navigationControl.getDistance();
            float currentAzimuth = helmControl.getCurrentHeading();
            float deltaAzimuth = deltaAngleDeg(currentAzimuth, azimuth);
            HelmControl.Speed speed = computeSpeed(deltaAzimuth, distance);
            helmControl.setSpeed(speed);
        } else {
            helmControl.setSpeed(HelmControl.Speed.STOP);
            nextWaypoint();
        }

        WarningControl.Danger danger = warningControl.getDanger();
        if (danger != null) {
            getManager().setCurrentBehaviour(EvadeBehaviour.class);
        }
    }

    @Override
    public void end() {
    }

    @Override
    public void event(Event event) {
    }
}

*/
