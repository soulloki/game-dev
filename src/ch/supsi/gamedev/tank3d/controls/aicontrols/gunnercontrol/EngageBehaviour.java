package ch.supsi.gamedev.tank3d.controls.aicontrols.gunnercontrol;

import ch.supsi.gamedev.tank3d.Event;
import ch.supsi.gamedev.tank3d.Globals;
import ch.supsi.gamedev.tank3d.controls.TankControl;
import ch.supsi.gamedev.tank3d.controls.aicontrols.Behaviour;
import ch.supsi.gamedev.tank3d.controls.aicontrols.FireSolutionControl;
import ch.supsi.gamedev.tank3d.controls.aicontrols.StabilityControl;
import ch.supsi.gamedev.tank3d.controls.aicontrols.WarningControl;
import com.jme3.math.FastMath;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;

public class EngageBehaviour extends Behaviour {

    private final TankControl tankControl;
    private final WarningControl warningControl;
    private final FireSolutionControl fireSolutionControl;
    private final StabilityControl stabilityControl;
    private Spatial target = null;
    private float fireRate = 0;

    public EngageBehaviour(TankControl tankControl, WarningControl warningControl, FireSolutionControl fireSolutionControl, StabilityControl stabilityControl) {
        this.tankControl = tankControl;
        this.warningControl = warningControl;
        this.fireSolutionControl = fireSolutionControl;
        this.stabilityControl = stabilityControl;
    }

    @Override
    public void start() {
        System.out.println("EngageBeahaviour attivato");
        fireRate = tankControl.getReloadTime() + FastMath.rand.nextFloat();

    }

    @Override
    public void loop(float tpf) {

        if (target == null) {
            target = warningControl.getAttacker();
            fireSolutionControl.setTarget(target);
            return;
        }
        if(targetIsDead()){
            target = null;
            getManager().setCurrentBehaviour(IdleBehaviour.class);
        }
        
        float azimutErr = FastMath.normalize(fireSolutionControl.getRange(), 0, 8) * FastMath.rand.nextFloat();
        float elevationErr = FastMath.normalize(fireSolutionControl.getRange(), 0, 5) * FastMath.rand.nextFloat();

        stabilityControl.setAzimuth(fireSolutionControl.getAzimuth() + azimutErr);
        stabilityControl.setElevation(fireSolutionControl.getElevation() + elevationErr);

        fireRate -= tpf;

        if (fireRate < 0) {
            tankControl.fire();
            fireRate = tankControl.getReloadTime() + FastMath.rand.nextFloat();
        }
        
        

    }

    @Override
    public void end() {
    }

    @Override
    public void event(Event event) {
    }
    
    private boolean targetIsDead(){
        Node root = Globals.SINGLETON.getRootNode();
        
        return !root.hasChild(target);
    }
}
