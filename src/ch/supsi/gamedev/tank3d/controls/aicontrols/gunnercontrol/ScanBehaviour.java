package ch.supsi.gamedev.tank3d.controls.aicontrols.gunnercontrol;

import ch.supsi.gamedev.tank3d.Event;
import ch.supsi.gamedev.tank3d.controls.aicontrols.Behaviour;
import ch.supsi.gamedev.tank3d.controls.aicontrols.StabilityControl;
import ch.supsi.gamedev.tank3d.controls.aicontrols.WarningControl;
import java.util.Random;

public class ScanBehaviour extends Behaviour {

    private final WarningControl warningControl;
    private final StabilityControl stabilityControl;
    private long counter = 0;

    public ScanBehaviour(WarningControl warningControl, StabilityControl stabilityControl) {
        this.warningControl = warningControl;
        this.stabilityControl = stabilityControl;
    }

    @Override
    public void start() {
        System.out.println("ScanBeahaviour attivato");
    }

    @Override
    public void loop(float tpf) {

        if (warningControl.getDanger() != null) {
            getManager().setCurrentBehaviour(EngageBehaviour.class);
        }
        ++counter;
        if (counter % 100 == 1) {
            Random rand = new Random();
            stabilityControl.setAzimuth(rand.nextFloat() * 359);
        }

    }

    @Override
    public void end() {
    }

    @Override
    public void event(Event event) {
    }
}
