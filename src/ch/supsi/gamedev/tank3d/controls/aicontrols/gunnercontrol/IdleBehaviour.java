package ch.supsi.gamedev.tank3d.controls.aicontrols.gunnercontrol;

import ch.supsi.gamedev.tank3d.Event;
import ch.supsi.gamedev.tank3d.controls.TankControl;
import ch.supsi.gamedev.tank3d.controls.aicontrols.Behaviour;
import ch.supsi.gamedev.tank3d.controls.aicontrols.StabilityControl;
import ch.supsi.gamedev.tank3d.controls.aicontrols.WarningControl;
import ch.supsi.gamedev.tank3d.controls.aicontrols.drivercontrol.EvadeBehaviour;
import com.jme3.scene.Spatial;
import java.util.Set;

public class IdleBehaviour extends Behaviour {

    private final TankControl tankControl;
    private final WarningControl warningControl;
    private final StabilityControl stabilityControl;

    public IdleBehaviour(TankControl tankControl, WarningControl warningControl, StabilityControl stabilityControl) {
        this.tankControl = tankControl;
        this.warningControl = warningControl;
        this.stabilityControl = stabilityControl;
    }

    @Override
    public void start() {
        System.out.println("IdleBeahaviour attivato");
    }

    @Override
    public void loop(float tpf) {

        float turretAngle = tankControl.getTurretAngle();
        float cannonElevation = tankControl.getCannonElevation();

        if (turretAngle != 0) {
            tankControl.rotateTurret(-turretAngle);
        }
        if (cannonElevation != 0) {
            tankControl.rotateCannon(-cannonElevation);
        }

        if (warningControl.getDanger() != null) {
            getManager().setCurrentBehaviour(EngageBehaviour.class);
        }

        if (!warningControl.projectiles().isEmpty()) {
            getManager().setCurrentBehaviour(ScanBehaviour.class);
        }

    }

    @Override
    public void end() {
    }

    @Override
    public void event(Event event) {
    }
}
